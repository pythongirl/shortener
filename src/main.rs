#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate rocket;
extern crate crockford;
extern crate rand;
extern crate rocket_contrib;
extern crate postgres;
extern crate serde;
extern crate time;

use std::env;
use std::sync::Mutex;

mod pages;
mod shortcut;

mod globals;
use globals::*;

mod api;

#[get("/<shortcut_id>")]
fn take_shortcut(
    shortcut_id: String,
    conn: rocket::State<DbConn>,
) -> Result<rocket::response::Redirect> {
    let decoded_id = crockford::decode(shortcut_id)? as i64;

    let mut conn = conn.lock().expect(DB_LOCK_ERROR);

    let destination_url: String = conn.query::<str>(
        "select url from shortcuts where id = $1",
        &[&decoded_id],
    )?.get(0).ok_or(Error::NoResults)?.try_get(0)?;

    conn.execute(
        "update shortcuts set clicks = clicks + 1 where id = $1",
        &[&decoded_id],
    )?;

    Ok(rocket::response::Redirect::to(destination_url))
}

fn init_db(conn: &mut postgres::Client) -> Result<()> {
    conn.execute(
        "create table if not exists shortcuts (
			id			integer primary key,
			url			text not null,
			clicks      integer not null default 0,
			owner		bigint not null
		)",
        &[],
    )?;

    Ok(())
}

fn connect_db() -> Result<postgres::Client> {
    dotenv::dotenv().ok();

    let conn = postgres::Client::connect(
        &env::var("DATABASE_URL").expect("DATABASE_URL must be set to the postgres connection config."),
        postgres::NoTls
    )?;

    Ok(conn)
}

fn make_rocket() -> Result<rocket::Rocket> {

    let mut conn = connect_db().expect("Unable to connect to DB");

    init_db(&mut conn).unwrap();

    Ok(rocket::ignite().manage(Mutex::new(conn)).mount(
        "/",
        routes![
            take_shortcut,
            pages::main_page,
            pages::main_script,
            pages::main_style,
            pages::favicon,
            api::list_shortcuts,
            api::request_new_shortcut
        ],
    ))
}

fn main() {
    make_rocket().unwrap().launch();
}
