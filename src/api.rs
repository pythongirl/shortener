use rocket::http::Cookies;
use rocket_contrib::json::Json;
use serde::{Deserialize, Serialize};

use crate::globals::*;
use crate::shortcut::Shortcut;

#[derive(Deserialize)]
pub struct ShortcutRequest {
    url: String,
}

#[derive(Serialize)]
pub struct ShortcutResponse {
    shortcut_id: String,
    destination_url: String,
    clicks: i32,
}

impl ShortcutResponse {
    pub fn from_shortcut(shortcut: Shortcut) -> ShortcutResponse {
        ShortcutResponse {
            shortcut_id: format!("{:0>3}", crockford::encode(shortcut.id as u64)),
            destination_url: shortcut.url,
            clicks: shortcut.clicks,
        }
    }
}

fn get_client_id(cookies: &Cookies) -> Result<u32> {
    Ok(crockford::decode(cookies.get("client_id").map(|c| c.value()).unwrap_or("0"))? as u32)
}

#[post("/", format = "application/json", data = "<shortcut_request>")]
pub fn request_new_shortcut(
    shortcut_request: Json<ShortcutRequest>,
    conn: rocket::State<DbConn>,
    cookies: Cookies,
) -> Result<Json<ShortcutResponse>> {
    let mut conn = conn.lock().expect(DB_LOCK_ERROR);

    let mut random_id: i32;

    loop {
        random_id = (rand::random::<u64>() % (1 << 15)) as i32;

        let id_taken: bool = conn.query::<str>(
            "select exists(select 1 from shortcuts where id = $1)",
            &[&random_id],
        )?[0].get(0);

        if !id_taken {
            break;
        }
    }

    let client_id = get_client_id(&cookies)?;

    let shortcut = Shortcut::new(random_id, shortcut_request.url.clone(), 0, i64::from(client_id));

    shortcut.add_to_db(&mut conn)?;

    Ok(Json(ShortcutResponse::from_shortcut(shortcut)))
}

#[derive(Serialize)]
pub struct ShortcutList {
    shortcuts: Vec<ShortcutResponse>,
}

#[get("/list")]
pub fn list_shortcuts(conn: rocket::State<DbConn>, cookies: Cookies) -> Result<Json<ShortcutList>> {
    let mut conn = conn.lock().expect(DB_LOCK_ERROR);

    let client_id = get_client_id(&cookies)?;

    // let mut stmt = conn.prepare("select id, url, clicks, owner from shortcuts where owner = ?")?;

    // let shortcuts = stmt
    //     .query_map(&[&client_id], |row| Shortcut::from_row(row))?
    //     .map(|r| r.unwrap());

    let results = conn.query(
        "select id, url, clicks, owner from shortcuts where owner = $1",
        &[&i64::from(client_id)])?;

    let shortcuts = results.iter().map(|row| Shortcut::from_row(row));

    let shortcut_responses = shortcuts.map(ShortcutResponse::from_shortcut);

    Ok(Json(ShortcutList {
        shortcuts: shortcut_responses.collect(),
    }))
}
