use rocket::http::Status;
use std::sync::Mutex;

pub type DbConn = Mutex<postgres::Client>;

pub const DB_LOCK_ERROR: &str = "DB Lock Error";

pub enum Error {
    CrockfordError(crockford::Error),
    PostgresError(postgres::Error),
    NoResults,
}

impl From<crockford::Error> for Error {
    fn from(error: crockford::Error) -> Self {
        Error::CrockfordError(error)
    }
}
impl From<postgres::Error> for Error {
    fn from(error: postgres::Error) -> Self {
        Error::PostgresError(error)
    }
}

impl<'r> rocket::response::Responder<'r> for Error {
    fn respond_to(self, _: &rocket::request::Request) -> rocket::response::Result<'r> {
        println!("{:?}", self);

        Err(match self {
            Error::CrockfordError(_) => Status::NotFound,
            Error::PostgresError(_) => Status::InternalServerError,
            Error::NoResults => Status::NotFound,
        })
    }
}

impl std::fmt::Debug for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Error::CrockfordError(e) => write!(f, "CrockfordError({:?})", e),
            Error::PostgresError(e) => write!(f, "PostgresError({:?})", e),
            Error::NoResults => write!(f, "NoResults")
        }
    }
}

pub type Result<T> = std::result::Result<T, Error>;
